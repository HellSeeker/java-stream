package in.abhishekkumarsingh.streamapi;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Hello world!
 *
 */
public class App {
	static List<Employee> employees = new ArrayList<>();

	static {
		employees.add(new Employee("Gautam", "Rai", 5000.0, Arrays.asList("Project 1", "Project 2")));
		employees.add(new Employee("Abhishek", "Singh", 6000.0, Arrays.asList("Project 1", "Project 3")));
		employees.add(new Employee("Devesh", "Rakheja", 7000.0, Arrays.asList("Project 1", "Project 4")));
		employees.add(new Employee("Surya", "Prakash", 8000.0, Arrays.asList("Project 4", "Project 2")));
		employees.add(new Employee("Ashish", "Garg", 9000.0, Arrays.asList("Project 3", "Project 2")));
	}

	public static void main(String[] args) {

		// forEach statement
		employees.stream().forEach(employee -> System.out.println(employee));

		// map
		// collect(toList or we can use toSet)
		List<Employee> increasedSalary = employees.stream()
				.map(e -> new Employee(e.getFirstName(), e.getLastName(), e.getSalary() * 1.10, e.getProjects()))
				.collect(Collectors.toList());
		System.out.println(increasedSalary);

		// filter
		List<Employee> filterEmployee = employees.stream().filter(e -> e.getSalary() > 5000.0)
				.map(e -> new Employee(e.getFirstName(), e.getLastName(), e.getSalary() * 1.10, e.getProjects()))
				.collect(Collectors.toList());

		System.out.println(filterEmployee);

		// findfirst
		Employee firstEmployee = employees.stream().filter(e -> e.getSalary() > 7000.0)
				.map(e -> new Employee(e.getFirstName(), e.getLastName(), e.getSalary() * 1.10, e.getProjects()))
				.findFirst().orElse(null);

		System.out.println(firstEmployee);

		// flatmap
		String projects = employees.stream().map(e -> e.getProjects()).flatMap(string -> string.stream())
				.collect(Collectors.joining(","));

		System.out.println(projects);

		// short circuit operators
		List<Employee> shortcircuitListOfEmployees = employees.stream().skip(1).limit(1).collect(Collectors.toList());
		System.out.println(shortcircuitListOfEmployees);

		// finite data
		Stream.generate(Math::random).limit(5).forEach(value -> System.out.println(value));

		// sorting
		List<Employee> sortedEmployees = employees.stream()
				.sorted((o1, o2) -> o1.getFirstName().compareToIgnoreCase(o2.getFirstName()))
				.collect(Collectors.toList());

		System.out.println(sortedEmployees);

		// min or max
		Employee maxSalaryEmployee = employees.stream().max(Comparator.comparing(Employee::getSalary))
				.orElseThrow(NoSuchElementException::new);
		System.out.println(maxSalaryEmployee);

		// reduce
		Double totalSalary = employees.stream().map(e->e.getSalary()).reduce(0.0,Double::sum);
		System.out.println(totalSalary);
	}
}
